# TODO
UI:
- New host view (for adding new hosts without having to edit the config file)
  - GetFreeId() method
- Sort hosts alphabetically
- Scrollable hosts list (with column headers always on top)
- Editor view: button to clone host as a new entry - e.g. for multiple users
  - Pass the clone to 'NewHost()'

Deployment:
- Packaging: AUR for Arch, SlackBuild
- The static jsoncpp library: Investigate/resolve gcc deprecation warnings

Other:
- Validate `hosts.json` before starting app to reduce undefined runtime behaviour if the config is broken

