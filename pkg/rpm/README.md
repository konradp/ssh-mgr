# Building RPM package
From the top-level of this repo, run the below.
This will build and run the Fedora Docker image of the RPM builder.
```bash
./pkg/rpm/docker_build.sh
```
Then, inside the container shell, build, test and publish the RPM package.
```bash
cd ./ssh-mgr/pkg/rpm/bin
./run.sh
```
That's it!

## Manual build
This script triggers these scripts, in this order:
- Source: run tests
- Source tarball
  This creates a source tarball, e.g. `/tmp/ssh-mgr/build/ssh-mgr-0.1.tar.gz`.
  ```bash
  ./tarball.sh
  ```
- Compile and test
  This extracts the source tarball, compiles it, and executes the application.
  ```bash
  ./compile_test.sh
  ```
- Build RPM
  This builds the RPM package using the source tarball and the SPEC file.
  ```bash
  ./build_rpm.sh
  ```
- Tests
  ```bash
  ./test_rpm.sh
  ```
- Upload RPM and tarball
  ```bash
  ./upload_rpm.sh
  ```

## Test commits to master
Only build the code at the latest commit that passed the tests

