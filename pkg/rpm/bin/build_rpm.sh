#!/bin/bash

# This script builds the RPM packages for common architectures
# Include config
. config

echo "Building RPM for `arch` architecture."

# Copy artifacts: tarball and SPEC file
cp $build_dir/ssh-mgr-*.tar.gz ~/rpmbuild/SOURCES/
cp $work_dir/pkg/rpm/ssh-mgr.spec ~/rpmbuild/SPECS/

# Build source RPM, keep resulting filepath
src_rpm=$(rpmbuild -bs ~/rpmbuild/SPECS/ssh-mgr.spec)
src_rpm=${src_rpm#Wrote: }
echo "The SRPM has been written to $src_rpm."


# Build source RPM, keep resulting filepath
bin_rpm=$(rpmbuild -bb ~/rpmbuild/SPECS/ssh-mgr.spec)
bin_rpm=${bin_rpm#Wrote: }
echo "The RPM has been written to $bin_rpm."

