#!/bin/bash

# Include config
. config

# Extract tarball
cd "$build_dir"
filename=$(ls ssh-mgr-*.tar.gz)
dirname="${filename%.tar.gz}"
tar -xf $filename
cd $dirname

# Compile and run
./configure
make
cd src && ./ssh-mgr

