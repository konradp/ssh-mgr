#!/bin/bash

# This is the main script for building RPM package.
# Run this inside the Docker container to compile, build, publish the package.

# Include config
. config

# Create source tarball
$bin_dir/tarball.sh

# Extract tarball, compile, execute
$bin_dir/compile_test.sh

# Build RPM
$bin_dir/build_rpm.sh

# Test RPM
#$bin_dir/test_rpm.sh

# Upload RPM and tarball
$bin_dir/upload_rpm.sh

