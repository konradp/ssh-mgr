#!/bin/bash

# Upload RPM packages
# Note: This currently outputs to the local directory which is git-tracked.
# TODO: This should upload to some website maybe.
# TODO: Also, do we publish the 'debuginfo' packages? These look useful.

# Include config
. config

for arch in $(ls -1 ~/rpmbuild/RPMS); do
    for f in $(ls -1 ~/rpmbuild/RPMS/$arch/*.rpm | grep -v "debuginfo"); do
        echo "Copying"
        echo "$f"
        cp "$f" "$work_dir/pkg/rpm/out/"
    done
done
