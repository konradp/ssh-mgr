#!/bin/bash

# Include config
. config

# Create a .tar.gz tarball
cd $work_dir
./autogen.sh
rm -rf "$work_dir/build"
mkdir "$work_dir/build"
cd "$work_dir/build"
../configure
make dist

