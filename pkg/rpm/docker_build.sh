#!/bin/bash
# Run this from top-level of this repo
# Note: Building takes about 6 minutes
docker build -t ssh-mgr-builder -f pkg/rpm/Dockerfile_centos .

# Note: search for "mount --bind inside Docker container" for a more secure solution
# than "--cap-add=SYS_ADMIN"
docker run -v `pwd`:/tmp/ssh-mgr -it --cap-add=SYS_ADMIN ssh-mgr-builder /bin/bash

