Name:           ssh-mgr
Version:        0.1
Release:        1%{?dist}
Summary:        An ncurses based ssh manager

License:        GPLv3+
URL:            http://example.com/%{name}
Source0:        http://example.com/%{name}-%{version}.tar.gz

%define targetdir /usr
%define bindir %{targetdir}/bin
%define includedir %{targetdir}/include

BuildRequires: ncurses-devel
Requires: ncurses

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%makeinstall
#make install PREFIX=%{targetdir}

%files
%{bindir}/ssh-mgr
%{includedir}/App.h
%{includedir}/ConfigInterface.h
%{includedir}/EditWin.h
%{includedir}/HelpWin.h
%{includedir}/NewWin.h
%{includedir}/Support.h
%{includedir}/json.h
#%license add-license-file-here
#%doc add-docs-here



%changelog
* Sun Oct 15 2017 root
- 
