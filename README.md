# ssh-mgr
An ncurses based text mode SSH connection manager for spawning SSH connections (e.g. xterm).

![ssh-mgr screenshot](screenshot.png?raw=true" "ssh-mgr with i3 window manager")

## Description
- Keeps your SSH host list organised and accessible.
- Spawns multiple connections from a single application.
- Utilises your favourite window manager for tabbed/tiled/stacked windows.
- Hosts list and configuration are stored in a single easy to manage `JSON` file.

## Dependencies
- `ncurses` (including `menu` and `form` libraries)

The compilation dependencies are the same as these defined in the `docker/` dir (`ncurses-develop` package required).

## Installation and usage
Clone this project's repository
```BASH
    git clone git@gitlab.com:nickyfow/ssh-mgr.git
    cd ssh-mgr
```

Compile, install, and run
```BASH
    ./autogen.sh
    mkdir build && cd build
    ../configure
    make
    sudo make install
    ssh-mgr
```

## Configuration
A brand new install will fail due to lack of a config file. 
To create one, you can use the template file as follows. 

```BASH
    mkdir -p ~/.config/ssh-mgr
    cp /usr/local/share/ssh-mgr/hosts.json ~/.config/ssh-mgr/
```

To add a new host, manually edit the `hosts.json` file to add a new entry. (see `TODO`)

