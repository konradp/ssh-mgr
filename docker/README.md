Have a look at the Dockerfiles for exact package dependencies for compiling and running the app.
Note: These steps are for CentOS. For other distros, simply use the Dockerfile of choice.
Note: These Dockerfiles are all for `armv7` architecture. To use Docker images images for e.g. `x86/x86-64`, replace the first `FROM` line in `Dockerfile` with an image of your choice.

```bash
cd ..
docker build -t ssh-mgr-centos -f docker/Dockerfile_centos .
docker run -it ssh-mgr-centos /bin/bash
cd /tmp/ssh-mgr/src
./compile
./ssh-mgr
```
