#include <algorithm>
#include <fstream>
#include "ConfigInterface.h"

ConfigInterface::ConfigInterface(std::string filePath)
: cfgFile_(filePath)
{
}

std::string
ConfigInterface::GetTermCmd()
{
    return GetConfigJson()["config"]["termCmd"].asString();
}


Host
ConfigInterface::GetHostById(int id)
{
    Json::Value v = GetConfigJson()["hosts"];

    for(size_t i = 0; i < v.size(); i++) {
        // Hosts
        if(v[i]["id"].asInt() == id) {
            // If found a host with the id
            return Host {
                v[i]["id"].asInt(),
                v[i]["name"].asString(),
                v[i]["addr"].asString(),
                v[i]["user"].asString(),
                v[i]["path"].asString()
            };
        }
    } //for
    // TODO: Handle exceptions
    throw std::exception();
}


//std::vector<Host>
View
ConfigInterface::GetHostsForPath(std::string path)
{
    Json::Value v = GetConfigJson()["hosts"];

    // Return vars
    std::vector<Host> hosts;
    std::vector<std::string> groups;
    View view;

    // Helper vars
    std::vector<std::string> pV = Support::StringSplit("/", path);

    // Get hosts list as JSON and repad into a vector of Host struct
    // (this is for the new_item later in ncurses)
    std::string                 t;          // Full path string
    std::vector<std::string>    tV;         // Split path
    std::vector<std::string>    tParentV;   // Split path of parent group

    for(size_t i = 0; i < v.size(); i++) {
        // Groups
        // Get parent group
        t = v[i]["path"].asString();
        tV = Support::StringSplit("/", t);
        tParentV = tV;
        tParentV.pop_back();

        if( (pV == tParentV)
            && !(std::find(groups.begin(), groups.end(), t) != groups.end())
        ) {
            // If a subfolder, and not yet present, add to list of groups
            groups.push_back(t);
        }

        // Hosts
        if(tV == pV) {
            // If host is in currently viewed group, add it
            hosts.push_back(Host {
                v[i]["id"].asInt(),
                v[i]["name"].asString(),
                v[i]["addr"].asString(),
                v[i]["user"].asString(),
                v[i]["path"].asString()
            });
        }
    } //for

    return View { hosts, groups };
}

/* Private */
Json::Value
ConfigInterface::GetConfigJson()
{
    std::ifstream f(cfgFile_, std::ifstream::binary);
    Json::Value cfg;
    f >> cfg;
    return cfg;
}

void
ConfigInterface::SetHost(Host h)
{
    Json::Value root = GetConfigJson();
    Json::Value v = root["hosts"];

    for(size_t i = 0; i < v.size(); i++) {
        if(v[i]["id"] == h.id) {
            v[i]["name"] = h.name;
            v[i]["addr"] = h.addr;
            v[i]["user"] = h.user;
            v[i]["path"] = h.path;
            root["hosts"] = v;
            break;
        }
    }
    std::ofstream f;
    f.open(cfgFile_);
    f << root;
    f.close();

    return;
}

