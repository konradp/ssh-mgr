#include <fstream>
#include "Support.h"

namespace Support {

void
Log(std::string s) {
    std::ofstream f;
    f.open("ssh-mgr.log", std::ofstream::app);
    f << s << "\n";
    f.close();
    return;
}

std::vector<std::string>
StringSplit(std::string delimiter, std::string text)
{
    std::vector<std::string> v;
    size_t m = text.find(delimiter);
    while(m != std::string::npos) {
	v.push_back(text.substr(0, m)); // text until delimiter
	text = text.substr(m + delimiter.size(), std::string::npos); // rest
	m = text.find(delimiter); // update position
    }
    if(text.size() > 0) v.push_back(text);
    return v;
};

}; // namespace Support

