#ifndef SUPPORT_H
#define SUPPORT_H

#include <string>
#include <vector>
#include <iostream>

struct Host {
    int id;
    std::string name;
    std::string addr;
    std::string user;
    std::string path;
};

struct View {
    std::vector<Host> hosts;
    std::vector<std::string> groups;
};

namespace Support {
    void    Log(std::string);
    std::vector<std::string>
    StringSplit(std::string delimiter, std::string text);
}; // namespace Support

#endif // SUPPORT_H

