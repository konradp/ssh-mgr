#include <algorithm>
//include <cstdlib> // for getenv
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <unistd.h> // for getenv, getpwuid, getuid
#include <sys/types.h> // for getenv, getpwuid, getuid
#include <pwd.h> // for getenv, getpwuid, getuid
#include <vector>

#include "App.h"
#include "lib/Support.h"

App::App()
: termCmd_("xterm -e /bin/bash -c"),
  curGrp_("/"),
  redrawMenu_(true)
{
    // Init ncurses and menu vars
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);

    // Get home dir
    std::string homedir = (getenv("HOME") == NULL)?
        getpwuid(getuid())->pw_dir : getenv("HOME");

    // Use config from homedir or current dir
    std::fstream f;
    cfgFile_ = homedir + "/.config/ssh-mgr/hosts.json";
    f = std::fstream(cfgFile_.c_str());
    endwin();
    if(!f.good()) {
        // Config not found in home dir
        cfgFile_ = "hosts.json";
        f = std::fstream(cfgFile_.c_str());
        if(!f.good()) {
            // Config not found in current dir
            endwin();
            std::cerr << "Error: Could not find the hosts.json config." << std::endl;
            std::cerr << "Error: You can copy a template from /usr/local/share/ssh-mgr/hosts.json." << std::endl;
            std::cerr << "Error: Place it in ~/.config/ssh-mgr/hosts.json." << std::endl;
            exit(1);
        }
    }

    // Read runtime config
    ReadConfig();
}

void
App::ConnectHost(std::string host, std::string user)
{
    // Prepare ssh command
    // Example: xterm -bg black -fg white -c 'ssh user@host'
    std::string cmd;

    // If user was specified, use it
    if(user != "")
        cmd = termCmd_ + " \"ssh " + user + "@" + host + "\"&";
    else
        cmd = termCmd_ + " \"ssh " + host + "\"&";
    
    // Execute ssh command
    std::system(cmd.c_str());
    return;
}

void
App::DrawMenu()
{
    while(redrawMenu_) {
        ReadHosts(); // Re-read config
        RunWindow(); // Redraw menu
    }
}

void
App::ReadConfig()
{
    // Read config and set variables
    ConfigInterface cfg = ConfigInterface(cfgFile_);
    termCmd_ = cfg.GetTermCmd();
}


// Parse the config file, and read the hosts/groups
//which should be displayed in current view
void
App::ReadHosts()
{
    // Clear current view(list of groups/hosts)
    view_.groups.clear();
    view_.hosts.clear();

    // Get a list of groups/hosts
    ConfigInterface cfg = ConfigInterface(cfgFile_);
    view_ = cfg.GetHostsForPath(curGrp_);
}

void
App::RunWindow()
{
    //MENU *menu;
    int ch;
    redrawMenu_ = false;
    clear();
    
    // Get hosts/groups list
    std::vector<Host> hosts = view_.hosts;
    std::vector<std::string> groups = view_.groups;
    Support::Log("Hosts: "+std::to_string(hosts.size())); // DEBUG

    // Menu items
    static std::vector<ITEM*> items(hosts.size() + groups.size() + 2);

    // Add hosts to menu
    // Also, set user pointer associated with each item (additional data from Host struct)
    for(size_t i = 0; i < hosts.size(); i++) {
        items[i] = new_item(
            hosts[i].name.c_str(),
            hosts[i].addr.c_str()
        );
        set_item_userptr(items[i], &hosts[i]);
    }

    // Add groups to menu
    // Also, set user pointer associated with each item (additional data from Group struct)
    for(size_t i = 0; i < groups.size(); i++) {
        items[i + hosts.size()] = new_item(groups[i].c_str(), NULL);
        set_item_userptr(items[i + hosts.size()], &groups[i]);
    }

    //Support::Log("Items: " + std::to_string(items.size())); // DEBUG
    items[groups.size() + hosts.size()] = (ITEM *) NULL; // end list with NULL
    //items.push_back((ITEM *) NULL); // End list with NULL

    // Add window and menu
    menu_win_ = newwin(LINES-2, COLS, 0, 1);
    menu_ = new_menu(&items[0]);
    set_menu_win(menu_, menu_win_);
    set_menu_sub(menu_, derwin(menu_win_, LINES-5, COLS-3, 3, 0));
    set_menu_format(menu_, 18, 1);
    set_menu_mark(menu_, "");
    mvwprintw(menu_win_, 0, 0, "Press: h: help, q: quit.");
    mvwprintw(menu_win_, 1, 0, "Location: %s", curGrp_.c_str());
    mvwhline(menu_win_, 2, 0, ACS_HLINE, 20);
    
    // Post menu
    post_menu(menu_);
    refresh();
    wrefresh(menu_win_);

    // Handle keyboard events
    while( (ch = getch()) != 'q' && (redrawMenu_ == false) ){
        switch(ch) {
            case KEY_DOWN:
                menu_driver(menu_, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(menu_, REQ_UP_ITEM);
                break;
            case 10: /* Enter */
            {
                // Get current item
                ITEM *cur = current_item(menu_);
                if((size_t) item_index(cur) < hosts.size()) {
                    // Selected a host
                    // data: A pointer to Host struct retrieved from the user pointer of the menu item
                    auto data = static_cast<Host*> (item_userptr(cur)); // see 'man menu_userptr'
                    ConnectHost((*data).addr, (*data).user); // Connect to the host
                    redrawMenu_ = true;
                } else {
                    // Selected a group
                    auto group = static_cast<std::string*> (item_userptr(cur)); // see 'man menu_userptr'
                    curGrp_ = *group; // navigate to current group
                    redrawMenu_ = true;
                    goto escape;
                }
                break;
            }
            case KEY_BACKSPACE:
            {
                // TODO: This needs to go only one level up, not all the way to the top
                std::vector<std::string> curGrpV = Support::StringSplit("/", curGrp_);
                curGrpV.pop_back();
                curGrp_ = "/";
                redrawMenu_ = true;
                goto escape;
            }
            case 'h': /* Help */
            {
                HelpWin helpWin = HelpWin();
                helpWin.Show(); // Note: this branches out
                redrawMenu_ = true;
                goto escape;
            }
            case 'e': /* Edit */
            {
                ITEM *cur = current_item(menu_);
                if((size_t) item_index(cur) >= groups.size()) {
                    // Clear current window
                    // Free menu items from memory
                    for(size_t i = 0; i < hosts.size() -1; i++)
                        free_item(items[i]);
                    free_menu(menu_);
                    endwin();
    
                    // Editing a host
                    Support::Log("editing a host"); // Debug
                    ITEM *cur = current_item(menu_);
                    auto data = static_cast<Host*> (item_userptr(cur)); // see 'man menu_userptr'
                    EditWin editWin = EditWin(*data);
                    editWin.cfgFile = cfgFile_;
                    editWin.Run();
                    redrawMenu_ = true;
                    goto escape;
                }
                break;
            }
            case 'n': /* New */
            {
                NewWin newWin = NewWin();
                newWin.cfgFile = cfgFile_;
                newWin.Run();
                redrawMenu_ = true;
                goto escape;
            }
        }
        refresh();
        wrefresh(menu_win_);
    }

    escape:
    // Free menu items from memory
    for(size_t i = 0; i < hosts.size() -1; i++)
        free_item(items[i]);
    free_menu(menu_);
    endwin();
    return;
}

