#ifndef CONFIG_INTERFACE_H
#define CONFIG_INTERFACE_H

#include <json/json.h>
//#include <json/value.h>
#include <string>
#include "lib/Support.h"

class ConfigInterface {
public:
    ConfigInterface(std::string filePath);

    // Get/set methods
    Host GetHostById(int id);
    View GetHostsForPath(std::string path);
    void SetHost(Host h);
    std::string GetTermCmd();

private:
    Json::Value GetConfigJson();
    std::string cfgFile_;
};

#endif // CONFIG_INTERFACE_H

