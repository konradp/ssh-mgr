#ifndef APP_H
#define APP_H

#include <json/json.h> // Really json/value.h and json/json.h
#include <menu.h> // implies ncurses.h
#include <ncurses.h>
#include <string>

#include "ConfigInterface.h"
#include "EditWin.h"
#include "HelpWin.h"
#include "NewWin.h"
#include "lib/Support.h"

class App {
public:
    App();
    void DrawMenu();
private:
    void            ConnectHost(std::string host, std::string user);
    void            ReadConfig();
    void            ReadHosts();
    void            RunWindow();

    // config variables
    std::string cfgFile_;
    std::string termCmd_;
    std::string curGrp_;

    // Windows
    View view_;
    bool redrawMenu_;

    // ncurses
    WINDOW *menu_win_;
    MENU *menu_;
};

#endif // APP_H

