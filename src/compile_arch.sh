g++ main.cc \
    App.cc \
    ConfigInterface.cc \
    EditWin.cc \
    HelpWin.cc \
    NewWin.cc \
    lib/Support.cc \
    -o ssh-mgr \
    -lform \
    -lmenu \
    -lncurses \
    -ljsoncpp \
    -I3rdparty \
    -g -std=c++11 -Wall
#./ssh-mgr
