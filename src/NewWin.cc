#include "NewWin.h"
#include <iostream>
#include <string.h> // for strnlen

NewWin::NewWin()
{
    // TODO: User tickbox: no user(current user)/specific user
    // TODO: Add 'copy' button: Simple functionality: pass to NewHost method
}

void
NewWin::Run()
{
    // Initialise curses
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(1);
    clear();

    // Initialise the fields
                        //h, w , y, x , offscreen, ?
    field_[0] = new_field(1, 20, 2, 14, 0, 0);
    field_[1] = new_field(1, 20, 4, 14, 0, 0);
    field_[2] = new_field(1, 20, 5, 14, 0, 0);
    field_[3] = new_field(1, 20, 6, 14, 0, 0);
    field_[4] = NULL;

	/* Set field options */
    for(int i = 0; i < 5; i++) {
	    set_field_back(field_[i], A_UNDERLINE); 	// Print a line for the option
	    field_opts_off(field_[i], O_AUTOSKIP);  	// Don't go to next field when field is filled up
    }

    // Set field values
    set_field_buffer(field_[0], 0, host_.name.c_str());
    set_field_buffer(field_[1], 0, host_.addr.c_str());
    set_field_buffer(field_[2], 0, host_.user.c_str());
    set_field_buffer(field_[3], 0, host_.path.c_str());

	/* Create the form and post it */
	form_ = new_form(field_);
	post_form(form_);
	refresh();

    mvprintw(0, 0, "Press F1: save, F2: cancel");
    mvhline(1, 0, ACS_HLINE, 20);
	
	mvprintw(2, 0, "Name:");
	mvprintw(4, 0, "Address:");
	mvprintw(5, 0, "User:");
    mvprintw(6, 0, "Path (group):");
	refresh();

	/* Loop through to get user requests */
    //int ch;
	while(int ch = getch()) {
        switch(ch)
		{	case KEY_DOWN:
				/* Go to next field */
				form_driver(form_, REQ_NEXT_FIELD);
				/* Go to the end of the present buffer */
				/* Leaves nicely at the last character */
				form_driver(form_, REQ_END_LINE);
				break;
			case KEY_UP:
				/* Go to previous field */
				form_driver(form_, REQ_PREV_FIELD);
				form_driver(form_, REQ_END_LINE);
				break;
            case KEY_LEFT:
                form_driver(form_, REQ_PREV_CHAR);
                break;
            case KEY_RIGHT:
                form_driver(form_, REQ_NEXT_CHAR);
                break;
            case KEY_BACKSPACE:
            case 127: /* Delete key */
                form_driver(form_, REQ_DEL_PREV);
                break;
            case KEY_F(1): /* Save */
                // Or the crrent field buffer won't save
                form_driver(form_, REQ_NEXT_FIELD);
                form_driver(form_, REQ_PREV_FIELD);
                SaveHost(Host {
                    host_.id,
                    TrimWhitespaces(field_buffer(field_[0], 0)),
                    TrimWhitespaces(field_buffer(field_[1], 0)),
                    TrimWhitespaces(field_buffer(field_[2], 0)),
                    TrimWhitespaces(field_buffer(field_[3], 0))
                });
                    
                goto escape;
                break;
            case KEY_F(2): /* Cancel */
                goto escape;
                break;
			default:
				/* If this is a normal character, it gets */
				/* Printed				  */	
				form_driver(form_, ch);
				break;
		}
	}

    escape:
	/* Un post form and free the memory */
	unpost_form(form_);
	//free_form(form_); // TODO: Why a crash here?
	free_field(field_[0]);
	free_field(field_[1]); 
	free_field(field_[2]); 
	free_field(field_[3]); 

	endwin();
	return;
}


/* PRIVATE */
void
NewWin::SaveHost(Host h)
{
    Support::Log("saving");
    // TODO: This should not be hardcoded
    ConfigInterface cfg = ConfigInterface(cfgFile);
    cfg.SetHost(h);

    return;
}

// see https://alan-mushi.github.io/2014/11/30/ncurses-forms.html
char*
NewWin::TrimWhitespaces(char* str)
{
    char* end;
    if(*str == 0) // all spaces?
        return str;

    // trim trailing space
    end = str + strnlen(str, 128) - 1;

    while(end > str && isspace(*end))
        end--;

    // write new null terminator
    *(end+1) = '\0';

    return str;
}

