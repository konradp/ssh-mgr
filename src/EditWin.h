#ifndef EDITWIN_H
#define EDITWIN_H

#include <form.h>
#include "ConfigInterface.h"
#include "lib/Support.h"

class EditWin {
public:
    EditWin(Host host);
    void Run();
    std::string cfgFile;

private:
    void  SaveHost(Host host);
    char* TrimWhitespaces(char* str);

    bool newHost_;
    Host host_;
    FIELD* field_[4];
    FORM* form_;
};

#endif // EDITWIN_H

