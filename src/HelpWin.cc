#include "HelpWin.h"
#include <string>
#include <vector>

HelpWin::HelpWin()
{}

void
HelpWin::Show()
{
    // Initialise curses
    initscr();
    noecho();
    clear();

    // Prepare help text
    std::vector<std::string> v {
        "h: Open/close help",
        "up/down arrows: Navigate the menu",
        "Enter: Open a connection, or go into a group",
        "e: Edit a host",
        "n: New host",
        "q: Quit"
    };

    // Print help text
    for(size_t i = 0; i < v.size(); i++) {
        mvprintw(i, 0, v[i].c_str());
    }

	// Loop until user presses 'h' again to exit the dialog
    int ch;
	while((ch = getch()) != 'h') {}
	endwin();
	return;
}

