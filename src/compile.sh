g++ main.cc \
    App.cc \
    ConfigInterface.cc \
    EditWin.cc \
    HelpWin.cc \
    NewWin.cc \
    lib/Support.cc \
    3rdparty/jsoncpp.cpp \
    -o ssh-mgr \
    -lform \
    -lmenu \
    -lncurses \
    -I3rdparty \
    -g -std=c++11 -Wall
#./ssh-mgr
